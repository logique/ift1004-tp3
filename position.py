__author__ = 'Mathieu Roy et Adam Roy'


class Position:
    """Une position à deux coordonnées: ligne et colonne. La convention utilisée est celle de la notation matricielle :
    le coin supérieur gauche d'une matrice est dénoté (0, 0) (ligne 0 et colonne 0). On additionne une unité de colonne
    lorsqu'on se déplace vers la droite, et une unité de ligne lorsqu'on se déplace vers le bas.

    +-------+-------+-------+-------+
    | (0,0) | (0,1) | (0,2) |  ...  |
    | (1,0) | (1,1) | (1,2) |  ...  |
    | (2,0) | (2,1) | (2,2) |  ...  |
    |  ...  |  ...  |  ---  |  ...  |
    +-------+-------+-------+-------+

    Attributes:
        ligne (int): La ligne associée à la position.
        colonne (int): La colonne associée à la position

    """
    def __init__(self, ligne, colonne):
        """Constructeur de la classe Position. Initialise les deux attributs de la classe.

        Args:
            ligne (int): La ligne à considérer dans l'instance de Position.
            colonne (int): La colonne à considérer dans l'instance de Position.

        """
        self.ligne = int(ligne)
        self.colonne = int(colonne)

    def positions_diagonales_bas(self):
        """Retourne une liste contenant les deux positions diagonales bas à partir de la position actuelle.

        Note:
            Dans cette méthode et les prochaines, vous n'avez pas à valider qu'une position est "valide", car dans le
            contexte de cette classe toutes les positions (même négatives) sont permises.

        Returns:
            list: La liste des deux positions.

        """

        # L'ensemble des positions est retourné sous forme d'une liste constituée de 2 positions. La position #1
        # correspond à la position diagonale bas gauche tandis que la position #2 correspond à la diagonale bas droite.

        return [Position(self.ligne + 1, self.colonne - 1), Position(self.ligne + 1, self.colonne + 1)]

    def positions_diagonales_haut(self):
        """Retourne une liste contenant les deux positions diagonales haut à partir de la position actuelle.

        Returns:
            list: La liste des deux positions.

        """

        # L'ensemble des positions est retourné sous forme d'une liste constituée de 2 positions. La position #1
        # correspond à la position diagonale haute gauche tandis que la position #2 correspond à la diagonale haute
        # droite.

        return [Position(self.ligne - 1, self.colonne - 1), Position(self.ligne - 1, self.colonne + 1)]

    def quatre_positions_diagonales(self):
        """Retourne une liste contenant les quatre positions diagonales à partir de la position actuelle.

        Returns:
            list: La liste des quatre positions.

        """

        # L'ensemble des positions est retourné sous forme d'une liste constituée de 4 positions. La position [0]
        # correspond à la position diagonale bas gauche. La position [1] correspond à la diagonale bas droite.
        # La position [2] correspond à la  diagonale haut gauche. La position [3] correspond à la diagonale haut
        # droite.

        return self.positions_diagonales_bas() + self.positions_diagonales_haut()

    def quatre_positions_sauts(self):
        """Retourne une liste contenant les quatre "sauts" diagonaux à partir de la position actuelle. Les positions
        retournées sont donc en diagonale avec la position actuelle, mais a une distance de 2.

        Returns:
            list: La liste des quatre positions.

        """

        # L'ensemble des positions est retourné sous forme d'une liste constituée de 4 sous-listes. La position [0]
        # correspond à la position saut bas gauche. La position [1] correspond à la position saut bas droit. La
        # position [2] correspond à la position saut haut gauche. La position [3] correspond à la position saut haut
        # droite.

        return [Position(self.ligne + 2, self.colonne - 2), Position(self.ligne + 2, self.colonne + 2),
                Position(self.ligne - 2, self.colonne - 2), Position(self.ligne - 2, self.colonne + 2)]

    def __eq__(self, other):
        """Méthode spéciale indiquant à Python comment vérifier si deux positions sont égales. On compare simplement
        la ligne et la colonne de l'objet actuel et de l'autre objet.

        """
        return self.ligne == other.ligne and self.colonne == other.colonne

    def __repr__(self):
        """Méthode spéciale indiquant à Python comment représenter une instance de Position par une chaîne de
        caractères. Notamment utilisé pour imprimer une position à l'écran.

        """
        return '({}, {})'.format(self.ligne, self.colonne)

    def __hash__(self):
        """Méthode spéciale indiquant à Python comment "hasher" une Position. Cette méthode est nécessaire si on veut
        utiliser une classe que nous avons définie nous mêmes comme clé d'un dictionnaire.

        """
        return hash(str(self))


if __name__ == '__main__':
    p1 = Position(1, 2)
    p2 = Position(3, 5)

    assert p1.positions_diagonales_bas()[0].ligne == 2, 'Erreur positions_diagonales_bas'
    assert p1.positions_diagonales_bas()[0].colonne == 1, 'Erreur positions_diagonales_bas'
    assert p1.positions_diagonales_bas()[1].ligne == 2, 'Erreur positions_diagonales_bas'
    assert p1.positions_diagonales_bas()[1].colonne == 3, 'Erreur positions_diagonales_bas'

    assert p1.positions_diagonales_haut()[0].ligne == 0, 'Erreur positions_diagonales_haut'
    assert p1.positions_diagonales_haut()[0].colonne == 1, 'Erreur positions_diagonales_haut'
    assert p1.positions_diagonales_haut()[1].ligne == 0, 'Erreur positions_diagonales_haut'
    assert p1.positions_diagonales_haut()[1].colonne == 3, 'Erreur positions_diagonales_haut'

    assert p1.quatre_positions_diagonales()[0].ligne == 2, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[0].colonne == 1, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[1].ligne == 2, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[1].colonne == 3, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[2].ligne == 0, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[2].colonne == 1, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[3].ligne == 0, 'Erreur quatre_positions_diagonales'
    assert p1.quatre_positions_diagonales()[3].colonne == 3, 'Erreur quatre_positions_diagonales'

    assert p2.quatre_positions_sauts()[0].ligne == 5, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[0].colonne == 3, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[1].ligne == 5, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[1].colonne == 7, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[2].ligne == 1, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[2].colonne == 3, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[3].ligne == 1, 'Erreur quatre_positions_sauts'
    assert p2.quatre_positions_sauts()[3].colonne == 7, 'Erreur quatre_positions_sauts'

    print('Tous les tests ont réussis!')
