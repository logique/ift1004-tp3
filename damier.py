__author__ = 'Mathieu Roy et Adam Roy'

from piece import *
from position import *


class Damier:
    """Plateau de jeu d'un jeu de dames. Contient un ensemble de pièces positionnées à une certaine position
    sur le plateau.

    Attributes:
        cases (dict): Dictionnaire dont une clé représente une Position, et une valeur correspond à la Piece
            positionnée à cet endroit sur le plateau.

        n_lignes (int): Le nombre de lignes du plateau. La valeur est 8 (constante).
        n_colonnes (int): Le nombre de colonnes du plateau. La valeur est 8 (constante).

    """

    def __init__(self):
        """Constructeur du Damier. Initialise un damier initial de 8 lignes par 8 colonnes.

        """
        self.n_lignes = 8
        self.n_colonnes = 8

        self.cases = {
            Position(7, 0): Piece("blanc", "pion"),
            Position(7, 2): Piece("blanc", "pion"),
            Position(7, 4): Piece("blanc", "pion"),
            Position(7, 6): Piece("blanc", "pion"),
            Position(6, 1): Piece("blanc", "pion"),
            Position(6, 3): Piece("blanc", "pion"),
            Position(6, 5): Piece("blanc", "pion"),
            Position(6, 7): Piece("blanc", "pion"),
            Position(5, 0): Piece("blanc", "pion"),
            Position(5, 2): Piece("blanc", "pion"),
            Position(5, 4): Piece("blanc", "pion"),
            Position(5, 6): Piece("blanc", "pion"),
            Position(2, 1): Piece("noir", "pion"),
            Position(2, 3): Piece("noir", "pion"),
            Position(2, 5): Piece("noir", "pion"),
            Position(2, 7): Piece("noir", "pion"),
            Position(1, 0): Piece("noir", "pion"),
            Position(1, 2): Piece("noir", "pion"),
            Position(1, 4): Piece("noir", "pion"),
            Position(1, 6): Piece("noir", "pion"),
            Position(0, 1): Piece("noir", "pion"),
            Position(0, 3): Piece("noir", "pion"),
            Position(0, 5): Piece("noir", "pion"),
            Position(0, 7): Piece("noir", "pion"),
        }

    def recuperer_piece_a_position(self, position):
        """Récupère une pièce dans le damier à partir d'une position.

        Args:
            position (Position): La position où récupérer la pièce.

        Returns:
            La pièce (de type Piece) à la position reçue en argument, ou None si aucune pièce n'était à cette position.

        """
        if position in self.cases:
            return self.cases[position]
        else:
            return None

    def position_est_dans_damier(self, position):
        """Vérifie si les coordonnées d'une position sont dans les bornes du damier (entre 0 inclusivement et le nombre
        de lignes/colonnes, exclusement.

        Args:
            position (Position): La position à valider.

        Returns:
            bool: True si la position est dans les bornes, False autrement.

        """
        return 0 <= position.ligne < 8 and 0 <= position.colonne < 8

    def piece_peut_se_deplacer_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut se déplacer à une certaine position cible.
        On parle ici d'un déplacement standard (et non une prise).

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce de type pion ne peut qu'avancer en diagonale (vers le haut pour une pièce blanche, vers le bas pour
        une pièce noire). Une pièce de type dame peut avancer sur n'importe quelle diagonale, peu importe sa couleur.
        Une pièce ne peut pas se déplacer sur une case déjà occupée par une autre pièce. Une pièce ne peut pas se
        déplacer à l'extérieur du damier.

        Args:
            position_piece (Position): La position de la pièce source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            bool: True si la pièce peut se déplacer à la position cible, False autrement.

        """
        if position_piece not in self.cases:
            return False
        if (self.cases[position_piece].est_noire() and self.cases[position_piece].est_pion()) \
                or (self.cases[position_piece].est_dame()):
            if position_cible in position_piece.positions_diagonales_bas():
                return (position_cible not in self.cases) and (self.position_est_dans_damier(position_cible))
            else:
                return False
        elif (self.cases[position_piece].est_blanche() and self.cases[position_piece].est_pion()) \
                or (self.cases[position_piece].est_dame()):
            if position_cible in position_piece.positions_diagonales_haut():
                return (position_cible not in self.cases) and (self.position_est_dans_damier(position_cible))
            else:
                return False

    def piece_peut_sauter_vers(self, position_piece, position_cible):
        """Cette méthode détermine si une pièce (à la position reçue) peut sauter vers une certaine position cible.
        On parle ici d'un déplacement qui "mange" une pièce adverse.

        Une pièce doit être positionnée à la position_piece reçue en argument (retourner False autrement).

        Une pièce ne peut que sauter de deux cases en diagonale. N'importe quel type de pièce (pion ou dame) peut sauter
        vers l'avant ou vers l'arrière. Une pièce ne peut pas sauter vers une case qui est déjà occupée par une autre
        pièce. Une pièce ne peut faire un saut que si elle saute par dessus une pièce de couleur adverse.

        Args:
            position_piece (Position): La position de la pièce source du saut.
            position_cible (Position): La position cible du saut.

        Returns:
            bool: True si la pièce peut sauter vers la position cible, False autrement.

        """
        if position_piece not in self.cases:
            return False
        if position_cible in position_piece.quatre_positions_sauts():
            position_intermediaire = Position((position_piece.ligne + position_cible.ligne) / 2,
                                              (position_piece.colonne + position_cible.colonne) / 2)
            if position_intermediaire in self.cases:
                # True si la pièce mangeur n'est pas de la meme couleur que la pièce mangée
                return self.cases[position_intermediaire].est_noire ^ self.cases[position_piece].est_noire
        return False

    def piece_peut_se_deplacer(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de se déplacer (sans faire de saut).

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre déplacements possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut se déplacer, False autrement.

        """
        # S'il n'a pas de pièce à la position_piece, il n'y a donc rien à déplacer.
        if position_piece not in self.cases:
            return False
        for position_cible in position_piece.quatre_positions_diagonales:
            if self.damier.piece_peut_se_deplacer_vers(position_piece, position_cible):
                return True
        return False

        ######################################## À MODIFIER si tu es d'accord #########################################
        # Adam:  à la ligne 161 self représente un objet quelconque de la classe Damier (donc qui n'est pas encore créé).
        # Puisqu'il n'y a aucun objet ou méthode damier de définit dans le code, le terme damier ne devrait pas
        # apparaître. Je suggère if self.piece_peut_se_deplacer_vers(position_piece, position_cible):
        ###############################################################################################################

    def piece_peut_faire_une_prise(self, position_piece):
        """Vérifie si une pièce à une certaine position a la possibilité de faire une prise.

        ATTENTION: N'oubliez pas qu'étant donné une position, il existe une méthode dans la classe Position retournant
        les positions des quatre sauts possibles.

        Args:
            position_piece (Position): La position source.

        Returns:
            bool: True si une pièce est à la position reçue et celle-ci peut faire une prise. False autrement.

        """
        if position_piece not in self.cases:
            return False
        for position_cible in position_piece.quatre_positions_sauts:
            if self.damier.piece_peut_sauter_vers(position_piece, position_cible):
                return True
        return False
        ######################################## À MODIFIER si tu es d'accord #########################################
        # Adam: à la ligne 187, même problème que la ligne 161
        ###############################################################################################################

    def piece_de_couleur_peut_se_deplacer(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de se déplacer
        vers une case adjacente (sans saut).

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un déplacement standard, False autrement.
        """
        for position in self.cases:
            if self.cases[position].couleur == couleur:
                if self.piece_peut_se_deplacer(position):
                    return True
        return False

    def piece_de_couleur_peut_faire_une_prise(self, couleur):
        """Vérifie si n'importe quelle pièce d'une certaine couleur reçue en argument a la possibilité de faire un
        saut, c'est à dire vérifie s'il existe une pièce d'une certaine couleur qui a la possibilité de prendre une
        pièce adverse.

        ATTENTION: Réutilisez les méthodes déjà programmées!

        Args:
            couleur (str): La couleur à vérifier.

        Returns:
            bool: True si une pièce de la couleur reçue peut faire un saut (une prise), False autrement.
        """
        for position in self.cases:
            if self.cases[position].couleur == couleur:
                if self.piece_peut_faire_une_prise(position):
                    return True
        return False

    def deplacer(self, position_source, position_cible):
        """Effectue le déplacement sur le damier. Si le déplacement est valide, on doit mettre à jour le dictionnaire
        self.cases, en déplaçant la pièce à sa nouvelle position (et possiblement en supprimant une adverse pièce qui a
        été prise).

        Cette méthode doit également:
        - Promouvoir un pion en dame si celui-ci atteint l'autre extrémité du plateau.
        - Retourner un message indiquant "ok", "prise" ou "erreur".

        ATTENTION: Si le déplacement est effectué, cette méthode doit retourner "ok" si aucune prise n'a été faite,
            et "prise" si une pièce a été prise.
        ATTENTION: Ne dupliquez pas de code! Vous avez déjà programmé (ou allez programmer) des méthodes permettant
            de valider si une pièce peut se déplacer vers un certain endroit ou non.

        Args:
            position_source (Position): La position source du déplacement.
            position_cible (Position): La position cible du déplacement.

        Returns:
            str: "ok" si le déplacement a été effectué sans prise, "prise" si une pièce adverse a été prise, et
                "erreur" autrement.

        """
        if position_source not in self.cases:
            return "erreur" # Il n'y a pas de pièce à bouger à cet endroit.
        couleur = self.cases[position_source].couleur
        if self.piece_de_couleur_peut_faire_une_prise(couleur):
            if not self.piece_peut_sauter_vers(position_piece, position_cible):
                return "erreur" # La capture est obligatoire si possible.
            else:
                self.cases[position_cible] = self.cases[position_source]
                del(self.cases[position_source])
                position_intermediaire = Position((position_source.ligne + position_cible.ligne) / 2,
                                              (position_source.colonne + position_cible.colonne) / 2)
                del(self.cases[position_intermediaire])
                if self.cases[position_source].type_de_piece == "pion" and \
                        (position_cible.ligne == 0) or position_cible.ligne == 7):
                    self.cases[position_cible].promouvoir()
                return "prise"
        elif self.piece_de_couleur_peut_se_deplacer(couleur): ############### Problème je pense
            self.cases[position_cible] = self.cases[position_source]
            del(self.cases[position_source])
            if self.cases[position_source].type_de_piece == "pion" and \
                    (position_cible.ligne == 0) or position_cible.ligne == 7):
                self.cases[position_cible].promouvoir()
                return "ok"
        else:
            return "erreur" # Il n'y a aucun mouvement possible.


        ######################################### À MODIFIER si tu es d'accord #########################################
        # Adam: à la ligne 270, la condition vérifie si N'IMPORTE QUEL PIECE de la couleur reçue en argument
        # a la possibilité de se déplacer vers une case adjacente. Si cela est vrai, le déplacement est effectué. Cela ne
        # vérifie pas si le déplacement demandé est possible.
        ###############################################################################################################

    def convertir_en_chaine(self):
        """Retourne une chaîne de caractères où chaque case est écrite sur une ligne distincte.
        Chaque ligne contient l'information suivante (respectez l'ordre et la manière de séparer les éléments):
        ligne,colonne,couleur,type

        Par exemple, un damier à deux pièces (un pion noir en (1, 2) et une dame blanche en (6, 1)) serait représenté
        par la chaîne suivante:

        1,2,noir,pion
        6,1,blanc,dame

        Cette méthode pourrait par la suite être réutilisée pour sauvegarder un damier dans un fichier.

        Returns:
            (str): La chaîne de caractères construite.

        """
        self.chaine = ""
        for ligne in range(0, 8):
            for colonne in range(0, 8):
                position = Position(ligne, colonne)
                if position in self.damier:
                    self.chaine.append(ligne, ',', colonne, ',', self.damier[position].couleur,
                                       self.damier[position].type_de_piece, '\n')

    def charger_dune_chaine(self, chaine):
        """Vide le damier actuel et le remplit avec les informations reçues dans une chaîne de caractères. Le format de
        la chaîne est expliqué dans la documentation de la méthode convertir_en_chaine.

        Args:
            chaine (str): La chaîne de caractères.

        """
        # TODO: À compléter
        # MATH : Je ne comprends pas... il faut vider self.cases ou l'affichage ou autre?

    def __repr__(self):
        """Cette méthode spéciale permet de modifier le comportement d'une instance de la classe Damier pour
        l'affichage. Faire un print(un_damier) affichera le damier à l'écran.

        """
        s = " +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+\n"
        for i in range(0, 8):
            s += str(i) + "| "
            for j in range(0, 8):
                if Position(i, j) in self.cases:
                    s += str(self.cases[Position(i, j)]) + " | "
                else:
                    s += "  | "
            s += "\n +---+---+---+---+---+---+---+---+\n"

        return s


if __name__ == "__main__":
    # Ceci n'est pas le point d'entrée du programme principal, mais il vous permettra de faire de petits tests avec
    # vos fonctions du damier.
    damier = Damier()
    print(damier)

    assert damier.recuperer_piece_a_position(Position(7, 0)).est_blanche(), 'Erreur recuperer_piece_a_position'
    assert not damier.recuperer_piece_a_position(Position(7, 0)).est_noire(), 'Erreur recuperer_piece_a_position'
    assert damier.recuperer_piece_a_position(Position(7, 0)).est_pion(), 'Erreur recuperer_piece_a_position'
    assert damier.recuperer_piece_a_position(Position(0, 7)).est_noire(), 'Erreur recuperer_piece_a_position'
    assert damier.recuperer_piece_a_position(Position(0, 7)).est_pion(), 'Erreur recuperer_piece_a_position'
    assert damier.recuperer_piece_a_position(Position(0, 8)) is None, 'Erreur recuperer_piece_a_position'

    assert damier.position_est_dans_damier(Position(0, 0)), 'Erreur position_est_dans_damier'
    assert damier.position_est_dans_damier(Position(4, 3)), 'Erreur position_est_dans_damier'
    assert damier.position_est_dans_damier(Position(7, 7)), 'Erreur position_est_dans_damier'
    assert not damier.position_est_dans_damier(Position(-2, 0)), 'Erreur position_est_dans_damier'
    assert not damier.position_est_dans_damier(Position(0, -1)), 'Erreur position_est_dans_damier'
    assert not damier.position_est_dans_damier(Position(8, 7)), 'Erreur position_est_dans_damier'
    assert not damier.position_est_dans_damier(Position(4, 9)), 'Erreur position_est_dans_damier'

    print(damier.piece_peut_se_deplacer_vers(Position(5, 0), Position(4, 1)))

    assert damier.piece_peut_se_deplacer_vers(Position(2, 1), Position(3, 2)), 'Erreur piece_peut_se_deplacer_vers'
    assert damier.piece_peut_se_deplacer_vers(Position(2, 1), Position(3, 0)), 'Erreur piece_peut_se_deplacer_vers'
    assert damier.piece_peut_se_deplacer_vers(Position(5, 0), Position(4, 1)), 'Erreur piece_peut_se_deplacer_vers'
    assert not damier.piece_peut_se_deplacer_vers(Position(5, 0), Position(4, -1)), 'Erreur piece_peut_se_deplacer_vers'
    assert not damier.piece_peut_se_deplacer_vers(Position(6, 1), Position(5, 2)), 'Erreur piece_peut_se_deplacer_vers'
    assert not damier.piece_peut_se_deplacer_vers(Position(2, 1), Position(2, 2)), 'Erreur piece_peut_se_deplacer_vers'
    assert not damier.piece_peut_se_deplacer_vers(Position(2, 1), Position(4, 3)), 'Erreur piece_peut_se_deplacer_vers'

    # Faire une partie test

    print('Tous les tests ont réussis!')