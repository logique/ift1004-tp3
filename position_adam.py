__author__ = 'Jean-Francis Roy'


class Position:
    """Une position à deux coordonnées: ligne et colonne. La convention utilisée est celle de la notation matricielle :
    le coin supérieur gauche d'une matrice est dénoté (0, 0) (ligne 0 et colonne 0). On additionne une unité de colonne
    lorsqu'on se déplace vers la droite, et une unité de ligne lorsqu'on se déplace vers le bas.

    +-------+-------+-------+-------+
    | (0,0) | (0,1) | (0,2) |  ...  |
    | (1,0) | (1,1) | (1,2) |  ...  |
    | (2,0) | (2,1) | (2,2) |  ...  |
    |  ...  |  ...  |  ---  |  ...  |
    +-------+-------+-------+-------+

    Attributes:
        ligne (int): La ligne associée à la position.
        colonne (int): La colonne associée à la position

    """


    def __init__(self, ligne, colonne):
        """Constructeur de la classe Position. Initialise les deux attributs de la classe.

        Args:
            ligne (int): La ligne à considérer dans l'instance de Position.
            colonne (int): La colonne à considérer dans l'instance de Position.

        """
        self.ligne = int(ligne)
        self.colonne = int(colonne)

    def positions_diagonales_bas(self):
        """Retourne une liste contenant les deux positions diagonales bas à partir de la position actuelle.

        Note:
            Dans cette méthode et les prochaines, vous n'avez pas à valider qu'une position est "valide", car dans le
            contexte de cette classe toutes les positions (même négatives) sont permises.

        Returns:
            list: La liste des deux positions.

        """

        # L'ensemble des positions est retourné sous forme d'une liste constituée de 2 sous-listes. La sous-liste #1
        # correspond à la position diagonale bas gauche tandis que la sous-liste #2 correspond à la diagonale bas droite.

        liste = [[self.ligne + 1, self.colonne - 1], [self.ligne + 1, self.colonne + 1]]
        return liste

    def positions_diagonales_haut(self):
        """Retourne une liste contenant les deux positions diagonales haut à partir de la position actuelle.

        Returns:
            list: La liste des deux positions.

        """
        # L'ensemble des positions est retourné sous forme d'une liste constituée de 2 sous-listes. La sous-liste #1
        # correspond à la position diagonale haute gauche tandis que la sous-liste #2 correspond à la diagonale haute
        # droite.

        liste = [[self.ligne - 1, self.colonne - 1], [self.ligne - 1, self.colonne + 1]]
        return liste

    def quatre_positions_diagonales(self):
        """Retourne une liste contenant les quatre positions diagonales à partir de la position actuelle.

        Returns:
            list: La liste des quatre positions.

        """
        # L'ensemble des positions est retourné sous forme d'une liste constituée de 4 sous-listes. La sous-liste [0]
        # correspond à la position diagonale haute gauche. La sous-liste [1] correspond à la diagonale haute droite.
        # La sous-liste [2] correspond à la  diagonale bas gauche. La sous-liste [3] correspond à la diagonale bas
        # droite.

        liste = self.positions_diagonales_haut() + self.positions_diagonales_bas()
        return liste

    def quatre_positions_sauts(self):
        """Retourne une liste contenant les quatre "sauts" diagonaux à partir de la position actuelle. Les positions
        retournées sont donc en diagonale avec la position actuelle, mais a une distance de 2.

        Returns:
            list: La liste des quatre positions.

        """
        # L'ensemble des positions est retourné sous forme d'une liste constituée de 4 sous-listes. La sous-liste [0]
        # correspond à la position saut haut gauche. La sous-liste [1] correspond à la position saut haut droit. La
        # sous-liste [2] correspond à la position saut bas gauche. La sous-liste [3] correspond à la position saut bas
        # droite.

        liste = [[self.ligne - 2, self.colonne - 2], [self.ligne - 2, self.colonne + 2], [self.ligne + 2, \
                self.colonne- 2], [self.ligne + 2, self.colonne + 2]]
        return liste


    def __eq__(self, other):
        """Méthode spéciale indiquant à Python comment vérifier si deux positions sont égales. On compare simplement
        la ligne et la colonne de l'objet actuel et de l'autre objet.

        """

        return self.ligne == other.ligne and self.colonne == other.colonne

    def __repr__(self):
        """Méthode spéciale indiquant à Python comment représenter une instance de Position par une chaîne de
        caractères. Notamment utilisé pour imprimer une position à l'écran.

        """
        return '({}, {})'.format(self.ligne, self.colonne)

    def __hash__(self):
        """Méthode spéciale indiquant à Python comment "hasher" une Position. Cette méthode est nécessaire si on veut
        utiliser une classe que nous avons définie nous mêmes comme clé d'un dictionnaire.

        """
        return hash(str(self))



if __name__ == "__main__":

    p1 = Position(4, 4)
    p2 = Position(7, 0)
    p3 = Position(0, 7)

    # Test de la méthode positions_diagonales_bas(). Toutes les positions du damier sont testée. Quelques test
    # unitaires ont été incorporés.
    n = 0

    while n <= 7:
        m = 0

        while m <= 7:
            position_test = Position(n, m)
            assert position_test.positions_diagonales_bas() == [[n + 1, m - 1], [n + 1, m + 1]], "Erreur position D.B."
            m += 1

        n += 1


    assert p1.positions_diagonales_bas() == [[5, 3], [5, 5]], "Erreur position D.B."
    assert p2.positions_diagonales_bas() == [[8, -1], [8, 1]], "Erreur position D.B."
    assert p3.positions_diagonales_bas() == [[1, 6], [1, 8]], "Erreur position D.B."


    # Test de la méthode positions_diagonales_haut(). Toutes les positions du damier sont testée. Quelques test
    # unitaires ont été incorporés.
    n = 0

    while n <= 7:
        m = 0

        while m <= 7:
            position_test = Position(n, m)
            assert position_test.positions_diagonales_haut() == [[n - 1, m - 1], [n - 1, m + 1]], "Erreur position D.H."
            m += 1

        n += 1

    assert p1.positions_diagonales_haut() == [[3, 3], [3, 5]], "Erreur position D.H."
    assert p2.positions_diagonales_haut() == [[6, -1], [6, 1]], "Erreur position D.H."
    assert p3.positions_diagonales_haut() == [[-1, 6], [-1, 8]], "Erreur position D.H."


    # Test de la méthode quatre_positions_diagonales(). Toutes les positions du damier sont testée. Quelques test
    # unitaires ont été incorporés.
    n = 0

    while n <= 7:
        m = 0

        while m <= 7:
            position_test = Position(n, m)
            assert position_test.quatre_positions_diagonales() == [[n - 1, m - 1], [n - 1, m + 1], [n + 1, m - 1], \
                                                                   [n + 1, m + 1]], "Erreur position 4 P.D."
            m += 1

        n += 1

    assert p1.quatre_positions_diagonales() == [[3, 3], [3, 5], [5, 3], [5, 5]], "Erreur position 4 P.D."
    assert p2.quatre_positions_diagonales() == [[6, -1], [6, 1], [8, -1], [8, 1]], "Erreur position 4 P.D."
    assert p3.quatre_positions_diagonales() == [[-1, 6], [-1, 8], [1, 6], [1, 8]], "Erreur position 4 P.D."


    # Test de la méthode quatre_positions_sauts(). Toutes les positions du damier sont testée. Quelques test
    # unitaires ont été incorporés.
    n = 0

    while n <= 7:
        m = 0

        while m <= 7:
            position_test = Position(n, m)
            assert position_test.quatre_positions_sauts() == [[n - 2, m - 2], [n - 2, m + 2], [n + 2, m - 2], \
                                                              [n + 2, m + 2]], "Erreur position 4 P.S."
            m += 1

        n += 1

    assert p1.quatre_positions_sauts() == [[2, 2], [2, 6], [6, 2], [6, 6]], "Erreur position 4 P.S."
    assert p2.quatre_positions_sauts() == [[5, -2], [5, 2], [9, -2], [9, 2]], "Erreur position 4 P.S."
    assert p3.quatre_positions_sauts() == [[-2, 5], [-2, 9], [2, 5], [2, 9]], "Erreur position 4 P.S."

    print("Tous les test ont réussie")